define({ "api": [
  {
    "type": "POST",
    "url": "/company/create",
    "title": "Создание компании",
    "version": "1.0.0",
    "name": "CreateCompany",
    "group": "company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "name",
            "description": "<p>Название компании</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "phone",
            "description": "<p>Номер телефона</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "address",
            "description": "<p>Адрес компании</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "email",
            "description": "<p>Электорнный адрес</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": true,
            "field": "website",
            "description": "<p>Сайт</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": true,
            "field": "description",
            "description": "<p>Описание</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": true,
            "field": "logo_url",
            "description": "<p>ссылка на картинку</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"name\": \"Test Company\",\n    \"phone\": \"899999999\",\n    \"address\": \"Test address\",\n    \"email\": \"email@test.ru\",\n    \"logo_url\": \"https://logo.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"Created company id\": 1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/company.py",
    "groupTitle": "company"
  },
  {
    "type": "DELETE",
    "url": "/company",
    "title": "Удаление компании",
    "version": "1.0.0",
    "name": "DeleteCompany",
    "group": "company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "company_id",
            "description": "<p>Идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\"company_id\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"Deleted company 3\": true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/company.py",
    "groupTitle": "company"
  },
  {
    "type": "GET",
    "url": "/company",
    "title": "Получение данных о компании",
    "version": "1.0.0",
    "name": "GetCompany",
    "group": "company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "company_id",
            "description": "<p>Идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\"company_id\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"phone\": \"899999999\",\n    \"name\": \"Test Company\",\n    \"added_on\": null,\n    \"description\": null,\n    \"email\": \"email@test.ru\",\n    \"company_id\": 1,\n    \"link\": null,\n    \"address\": \"Test address\",\n    \"logo_url\": \"https://logo.com\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/company.py",
    "groupTitle": "company"
  },
  {
    "type": "GET",
    "url": "/company/list",
    "title": "Получение списка неудалённых компаний",
    "version": "1.0.0",
    "name": "ListCompanies",
    "group": "company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "companies_per_page",
            "description": "<p>Колиечество компаний на странице</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>Номер страницы</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"companies_per_page\" : 2,\n    \"page\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"page\": 1,\n    \"companies\": [\n        {\n            \"company_id\": 4,\n            \"logo_url\": \"123\",\n            \"name\": \"OOO\",\n            \"email\": \"email@email.ru\",\n            \"address\": \"Moskovsky avenue\",\n            \"phone\": \"894444111\",\n            \"link\": null,\n            \"description\": null,\n            \"added_on\": 2020-06-15T09:37:02.752234\n        },\n        {\n            \"company_id\": 5,\n            \"logo_url\": \"https://logo.com\",\n            \"name\": \"Test Company\",\n            \"email\": \"email@test.ru\",\n            \"address\": \"Test address\",\n            \"phone\": \"899999999\",\n            \"link\": \"link.com\",\n            \"description\": null,\n            \"added_on\": 2020-06-15T09:51:48.257382\n        }\n    ],\n    \"companies_per_page\": 2\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/company.py",
    "groupTitle": "company"
  },
  {
    "type": "POST",
    "url": "/company",
    "title": "Обновление данных о компании",
    "version": "1.0.0",
    "name": "UpdateCompany",
    "group": "company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "company_id",
            "description": "<p>Идентификатор компании</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"company_id\": 1,\n    \"website\": \"website.test\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"logo_url\": \"https://logo.com\",\n    \"phone\": \"899999999\",\n    \"added_on\": null,\n    \"link\": \"website.test\",\n    \"email\": \"email@test.ru\",\n    \"company_id\": 1,\n    \"name\": \"Test Company\",\n    \"address\": \"Test address\",\n    \"description\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/company.py",
    "groupTitle": "company"
  },
  {
    "type": "POST",
    "url": "/user/create",
    "title": "Создание пользователя",
    "version": "1.0.0",
    "name": "CreateUser",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "last_name",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "gender",
            "description": "<p>Пол</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "email",
            "description": "<p>Почта</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": false,
            "field": "phone",
            "description": "<p>Номер телефона</p>"
          },
          {
            "group": "Parameter",
            "type": "str",
            "optional": true,
            "field": "website",
            "description": "<p>Сайт</p>"
          },
          {
            "group": "Parameter",
            "type": "List[int]",
            "optional": true,
            "field": "comapny_ids",
            "description": "<p>Список копаний в которые добавить пользователя</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"first_name\": \"Test\",\n    \"last_name\": \"Testovich\",\n    \"gender\": \"male\",\n    \"email\": \"test@test.ru\",\n    \"phone\": \"899912345\",\n    \"company_ids\": [5]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"Created user id\": 3\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/user.py",
    "groupTitle": "user"
  },
  {
    "type": "DELETE",
    "url": "/user",
    "title": "Удаление пользователя",
    "version": "1.0.0",
    "name": "DeleteUser",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "user_id",
            "description": "<p>Идентификатор пользователя</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"user_id\": \"3\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n\"Deleted user 3\": true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/user.py",
    "groupTitle": "user"
  },
  {
    "type": "POST",
    "url": "/user/employ",
    "title": "Добавление пользователя в компании",
    "version": "1.0.0",
    "name": "EmployUser",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "user_id",
            "description": "<p>Идентификатор пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "List[int]",
            "optional": false,
            "field": "company_ids",
            "description": "<p>Список компаний, в которые добавить пользователя</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "    {\n    \"user_id\": 3,\n    \"phone\": \"8999123456\",\n    \"email\": \"email@email.ru\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n\"last_name\": \"Testovich\",\n\"first_name\": \"Test\",\n\"phone\": \"8999123456\",\n\"email\": \"email@email.ru\",\n\"is_employed\": true,\n\"public_id\": 3,\n\"is_deleted\": false,\n\"gender\": \"male\",\n\"companies\": [\n        {\n            \"phone\": \"899999999\",\n            \"email\": \"email@test.ru\",\n            \"logo_url\": \"https://logo.com\",\n            \"name\": \"Test Company\",\n            \"description\": null,\n            \"company_id\": 5,\n            \"address\": \"Test address\",\n            \"added_on\": \"2020-06-15T09:51:48.257382\",\n            \"website\": \"website.com\"\n        },\n        {\n            \"phone\": \"1545454\",\n            \"email\": \"email@appkode.ru\",\n            \"logo_url\": null,\n            \"name\": \"Test Company\",\n            \"description\": null,\n            \"company_id\": 6,\n            \"address\": \"Kaliningrad\",\n            \"added_on\": \"2020-06-15T10:45:16.616582\",\n            \"website\": null\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/user.py",
    "groupTitle": "user"
  },
  {
    "type": "GET",
    "url": "/user",
    "title": "Получение пользователя",
    "version": "1.0.0",
    "name": "GetUser",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "user_id",
            "description": "<p>Идентификатор пользователя</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"first_name\": \"Test\",\n    \"last_name\": \"Testovich\",\n    \"gender\": \"male\",\n    \"email\": \"test@test.ru\",\n    \"phone\": \"899912345\",\n    \"company_ids\": [5]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n     \"is_employed\": true,\n     \"last_name\": \"Testovich\",\n     \"first_name\": \"Test\",\n     \"companies\": [\n         {\n             \"website\": \"website.com\",\n             \"company_id\": 5,\n             \"name\": \"Test Company\",\n             \"address\": \"Test address\",\n             \"added_on\": \"2020-06-15T09:51:48.257382\",\n             \"description\": null,\n             \"logo_url\": \"https://logo.com\",\n             \"phone\": \"899999999\",\n             \"email\": \"email@test.ru\"\n         }\n     ],\n     \"is_deleted\": false,\n     \"phone\": \"899912345\",\n     \"email\": \"test@test.ru\",\n     \"public_id\": 3,\n     \"gender\": \"male\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/user.py",
    "groupTitle": "user"
  },
  {
    "type": "GET",
    "url": "/user/list",
    "title": "Получение списка пользователей",
    "version": "1.0.0",
    "name": "ListUsers",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "pagination_offset",
            "description": "<p>Номер страницы</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "pagination_limit",
            "description": "<p>Количество записей на странице</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"pagination_offset\" : 0,\n    \"pagination_limit\": 2\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"pagination_offset\": 1,\n    \"users\": [\n                {\n            \"public_id\": 1,\n            \"phone\": \"855512345\",\n            \"is_deleted\": false,\n            \"email\": \"mb@meh.ru\",\n            \"gender\": \"female\",\n            \"is_employed\": false,\n            \"last_name\": \"Belozerova\",\n            \"companies\": null,\n            \"first_name\": \"Maria\"\n        },\n        {\n            \"public_id\": 2,\n            \"phone\": \"899912345\",\n            \"is_deleted\": false,\n            \"email\": \"test@test.ru\",\n            \"gender\": \"male\",\n            \"is_employed\": false,\n            \"last_name\": \"Testovich\",\n            \"companies\": null,\n            \"first_name\": \"Test\"\n        }\n\n    ],\n    \"pagination_limit\": 2\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/user.py",
    "groupTitle": "user"
  },
  {
    "type": "POST",
    "url": "/user",
    "title": "Обновление пользователя",
    "version": "1.0.0",
    "name": "UpdateUser",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "user_id",
            "description": "<p>Идентификатор пользователя</p>"
          },
          {
            "group": "Parameter",
            "optional": true,
            "field": "str",
            "description": "<p>first_name Имя</p>"
          },
          {
            "group": "Parameter",
            "optional": true,
            "field": "email",
            "description": "<p>phone Номер телефона</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "    {\n    \"user_id\": 3,\n    \"phone\": \"8999123456\",\n    \"email\": \"email@email.ru\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Response-Example:",
          "content": "{\n    \"gender\": \"male\",\n    \"first_name\": \"Test\",\n    \"companies\": [\n        {\n            \"website\": \"website.com\",\n            \"company_id\": 5,\n            \"name\": \"Test Company\",\n            \"address\": \"Test address\",\n            \"added_on\": \"2020-06-15T09:51:48.257382\",\n            \"description\": null,\n            \"logo_url\": \"https://logo.com\",\n            \"phone\": \"899999999\",\n            \"email\": \"email@test.ru\"\n\n        }\n    ],\n    \"is_deleted\": false,\n    \"is_employed\": true,\n    \"last_name\": \"Testovich\",\n    \"email\": \"email@email.ru\",\n    \"public_id\": 3,\n    \"phone\": \"8999123456\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "employment_api/endpoints/user.py",
    "groupTitle": "user"
  }
] });
