### Описание

Пример API, основанного на CRUD-ах двух сущностей: User и Company

### Pre requirements
* Docker
* Docker Compose
* python 3.8 (+dev)
* pipenv

##### Локальный просмотр APIDOC спецификации:
1) Открыть в браузере файл spec/index.html

`chromium-browser spec/index.html`

### Локальный запуск в виртуальном окружении
Данные для подключения к бд были захардкожены для упрощения запуска

1) Создать окружение с python 3.8, активировать его
2) Установить зависимости из requirements_dev.txt
3) Выполнить команду  ```make local``` - запуск postgres
4) `source ./env.env`
5) `flask db upgrade`
6) `flask run`
    
##### Генерация APIDOC спецификации:
1) `npm install apidoc -g`
2) `apidoc -i src -o spec`

##### Todo:
1) Автоматизация запуска, миграций. Dockerfile, entrypoint.sh
2) Тесты
3) Рефакторинг main файла, добавление спецификации в роутинг
4) Добавить логику на параметр is_employed (?)