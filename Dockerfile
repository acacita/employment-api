FROM python:3.8-alpine

ENV APP_ROOT /employment-api

RUN mkdir /employment-api
WORKDIR ${APP_ROOT}

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /usr/local/lib/python3.8/

RUN apk update
RUN apk add postgresql-dev gcc python3-dev musl-dev

COPY requirements_dev.txt ./requirements.txt
RUN pip install -r requirements.txt

ADD . ${APP_ROOT}

RUN ls .

EXPOSE 5000