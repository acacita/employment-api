from flask import Flask
from flask_migrate import Migrate  # type: ignore

from .extensions import db
from .settings import Config


def _initialize_error_handlers(application):
    """
    Инициализация обработчиков кастомных ошибок
    :param application:
    :return:
    """
    from employment_api.exceptions.handlers import ERRORS_BP

    application.register_blueprint(ERRORS_BP)


def _setup_blueprints(application):
    """
    Инициализация  blueprints
    :param application:
    :return:
    """
    from employment_api.endpoints import USER_BP, COMPANY_BP

    application.register_blueprint(USER_BP)
    application.register_blueprint(COMPANY_BP)


def create_app(config_object=Config):
    app = Flask(__name__)
    app.config.from_object(config_object)
    db.init_app(app)
    migrate = Migrate(app, db)  # noqa: F841
    _initialize_error_handlers(app)
    _setup_blueprints(app)
    return app


if __name__ == '__main__':
    create_app().run()
