from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional, List


@dataclass
class CompanyServiceData:
    """
    Данные, поступающие на вход для обработки слоем use_cases
    """

    name: str
    phone: str
    address: str
    email: str
    website: Optional[str] = None
    logo_url: Optional[str] = None
    description: Optional[str] = None
    company_id: Optional[int] = None
    added_on: Optional[datetime] = None


@dataclass
class CompanyResultServiceData:
    """
    Данные о компании после обработки
    """

    name: str
    phone: str
    address: str
    email: str
    company_id: Optional[int] = None
    link: Optional[str] = None
    logo_url: Optional[str] = None
    description: Optional[str] = None
    added_on: Optional[datetime] = None


@dataclass
class GetDeleteCompanyData:
    """
    Входные данные необходимые для получения/удаления компании
    """

    company_id: int


@dataclass
class UpdateCompanyServiceData:
    """
    Данные для запроса обновления компании
    """

    company_id: int
    website: Optional[str] = None
    logo_url: Optional[str] = None
    description: Optional[str] = None


@dataclass
class CompaniesListInput:
    """
    Входные данные для получения компаний с пагинацией
    """

    page: int = 0
    companies_per_page: int = 0


@dataclass
class CompaniesListServiceData:
    """
    Список всех неудалённых компаний
    """

    page: int = 0
    companies_per_page: int = 0
    companies: List[CompanyResultServiceData] = field(default_factory=list)
