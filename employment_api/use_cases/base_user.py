from datetime import datetime
from dataclasses import dataclass, field
from typing import Any, Optional
from typing import List

import marshmallow.validate

from .base_company import CompanyServiceData


@dataclass
class UserServiceData:
    first_name: str
    last_name: str
    gender: str = field(metadata={"validate": marshmallow.validate.OneOf({'male', 'female', 'na'})})
    email: str
    phone: str
    company_ids: List[Any] = field(default_factory=list)


@dataclass
class UserServiceUpdateData:
    user_id: int
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    phone: Optional[str] = None
    email: Optional[str] = None


@dataclass
class UserResultServiceData:
    public_id: int
    first_name: str
    last_name: str
    gender: str
    email: str
    phone: str
    is_employed: bool = False
    companies: Optional[List[CompanyServiceData]] = None
    is_deleted: bool = False


@dataclass
class UserEmploymentData:
    user_id: int
    company_ids: List[int] = field(default_factory=list)


@dataclass
class UserGetDeleteData:
    user_id: int


@dataclass
class ListUsersData:
    pagination_offset: int = 0
    pagination_limit: int = 0


@dataclass
class ListUsersServiceData:
    pagination_offset: int = 0
    pagination_limit: int = 0
    users: List[UserResultServiceData] = field(default_factory=list)
