from dataclasses import asdict
from typing import Dict, Any, List

from employment_api.storage import (
    CompanyStorageData,
    CompanyRepository,
)
from employment_api.storage import DatabaseSessionScope
from .base_company import (
    CompanyServiceData,
    CompanyResultServiceData,
    UpdateCompanyServiceData,
    CompaniesListInput,
    CompaniesListServiceData,
)


class CompanyUseCases:
    """
    Методы бизнес-логики для компаний
    """

    @staticmethod
    def create(data: CompanyServiceData) -> int:
        """
        Создание компании
        :param data:
        :return: str - идентификатор компании
        """
        db_data: CompanyStorageData = CompanyStorageData(**asdict(data))
        with DatabaseSessionScope() as db:
            company_repository: CompanyRepository = CompanyRepository(db)
            company_id: int = company_repository.create(db_data)
        return company_id

    @staticmethod
    def update(update_data: UpdateCompanyServiceData) -> CompanyResultServiceData:
        """
        Обновление данных о компании - в случае отсуствия данных обновления возвращает исходные данные
        :param update_data:
        :return:
        """
        database_update_data: Dict[str, Any] = {k: v for k, v in asdict(update_data).items() if v}
        company_id: int = database_update_data.pop('company_id')

        with DatabaseSessionScope() as db:
            company_repository: CompanyRepository = CompanyRepository(db)
            updated_data: CompanyStorageData = company_repository.update(company_id, database_update_data)
        return CompanyResultServiceData(
            email=updated_data.email,
            phone=updated_data.phone,
            link=updated_data.website,
            logo_url=updated_data.logo_url,
            address=updated_data.address,
            description=updated_data.description,
            name=updated_data.name,
            company_id=company_id,
        )

    @staticmethod
    def delete(company_id: int) -> bool:
        """
        Soft delete компании из бд
        :param company_id:
        :return: True если удаление прошло успешно
        """
        with DatabaseSessionScope() as db:
            company_repository: CompanyRepository = CompanyRepository(db)
            success: bool = company_repository.delete(public_id=company_id)
        return success

    @staticmethod
    def get(company_id: int) -> CompanyResultServiceData:
        """
        Получение компании из бд
        :param company_id:
        :return:
        """
        with DatabaseSessionScope() as db:
            company_repository: CompanyRepository = CompanyRepository(db)
            company: CompanyStorageData = company_repository.get(public_id=company_id)
        return CompanyResultServiceData(
            name=company.name,
            address=company.address,
            company_id=company_id,
            email=company.email,
            phone=company.phone,
            link=company.website,
            logo_url=company.logo_url,
            description=company.description,
        )

    @staticmethod
    def list_all(args: CompaniesListInput) -> CompaniesListServiceData:
        """
        Получение списка компаний с пагинацией из бд
        :param args:
        :return:
        """
        with DatabaseSessionScope() as db:
            company_repository: CompanyRepository = CompanyRepository(db)
            companies: List[CompanyStorageData] = company_repository.company_list(
                page_number=args.page, companies_per_page=args.companies_per_page
            )
            return CompaniesListServiceData(
                page=args.page,
                companies_per_page=args.companies_per_page,
                companies=[
                    CompanyResultServiceData(
                        name=company.name,
                        address=company.address,
                        company_id=company.company_id,
                        email=company.email,
                        phone=company.phone,
                        link=company.website,
                        logo_url=company.logo_url,
                        added_on=company.added_on,
                    )
                    for company in companies
                ],
            )
