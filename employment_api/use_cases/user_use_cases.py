from dataclasses import asdict
from typing import Dict, Any, List

from employment_api.storage import DatabaseSessionScope
from employment_api.storage.user_repository import UserRepository, UserStorageData
from .base_company import CompanyServiceData
from .base_user import (
    UserServiceData,
    UserResultServiceData,
    UserServiceUpdateData,
    UserEmploymentData,
    ListUsersData,
    ListUsersServiceData,
)


class UserUseCase:
    @staticmethod
    def create(data: UserServiceData) -> int:
        """
        Создание нового пользователя в бд и получение его id
        :param data:
        :return: str - id пользователя
        """
        create_data: UserStorageData = UserStorageData(**asdict(data))
        with DatabaseSessionScope() as db:
            user_repository: UserRepository = UserRepository(db)
            user_id: int = user_repository.create(create_data)

        return user_id

    @staticmethod
    def get(user_id: int) -> UserResultServiceData:
        """
        Получение данных о пользователе по его id
        :param user_id:
        :return:
        """
        with DatabaseSessionScope() as db:
            user_repository: UserRepository = UserRepository(db)
            user_data: UserStorageData = user_repository.get(public_id=user_id)
        return UserResultServiceData(
            email=user_data.email,
            first_name=user_data.first_name,
            last_name=user_data.last_name,
            gender=user_data.gender,
            phone=user_data.phone,
            public_id=user_data.public_id,
            is_employed=user_data.is_employed,
            companies=[CompanyServiceData(**asdict(company)) for company in user_data.companies]
            if user_data.companies
            else None,
        )

    @staticmethod
    def update(update_data: UserServiceUpdateData) -> UserResultServiceData:
        """
        Обновление данных о пользователе.
        Если не было обновления данных, возвращает неизменные данные о пользователе
        :param update_data:
        :return:
        """
        database_update_data: Dict[str, Any] = {k: v for k, v in asdict(update_data).items() if v}
        user_id: int = database_update_data.pop('user_id')
        if database_update_data.get('first_name'):
            database_update_data['firstname'] = database_update_data.pop('first_name')
        if database_update_data.get('last_name'):
            database_update_data['lastname'] = database_update_data.pop('last_name')

        with DatabaseSessionScope() as db:
            user_repository: UserRepository = UserRepository(db)
            user_data: UserStorageData = user_repository.update(user_id, database_update_data)
        return UserResultServiceData(
            email=user_data.email,
            first_name=user_data.first_name,
            last_name=user_data.last_name,
            gender=user_data.gender,
            phone=user_data.phone,
            public_id=user_data.public_id,
            is_employed=user_data.is_employed,
            companies=[CompanyServiceData(**asdict(company)) for company in user_data.companies]
            if user_data.companies
            else None,
        )

    @staticmethod
    def employ(employment_data: UserEmploymentData) -> UserResultServiceData:
        """
        Добавление пользователя в компании
        :param employment_data: id пользователя, список id компаний, в которых пользователь устроен
        :return:
        """
        with DatabaseSessionScope() as db:
            user_repository = UserRepository(db)
            user_data: UserStorageData = user_repository.employ(
                public_id=employment_data.user_id, company_ids=employment_data.company_ids
            )

        return UserResultServiceData(
            email=user_data.email,
            first_name=user_data.first_name,
            last_name=user_data.last_name,
            gender=user_data.gender,
            phone=user_data.phone,
            public_id=user_data.public_id,
            is_employed=user_data.is_employed,
            companies=[CompanyServiceData(**asdict(company)) for company in user_data.companies]
            if user_data.companies
            else None,
        )

    @staticmethod
    def delete(user_id: int) -> bool:
        """
        Soft-delete пользователя
        :param user_id:
        :return:
        """
        with DatabaseSessionScope() as db:
            user_repository = UserRepository(db)
            delete_result: bool = user_repository.delete(public_id=user_id)
        return delete_result

    @staticmethod
    def list_all(args: ListUsersData) -> ListUsersServiceData:
        """
        Список неудалённых пользователей с пагинацией
        :param args:
        :return:
        """
        with DatabaseSessionScope() as db:
            user_repository = UserRepository(db)
            users_list: List[UserStorageData] = user_repository.list_all(
                pagination_limit=args.pagination_limit, pagination_offset=args.pagination_offset
            )
        return ListUsersServiceData(
            pagination_limit=args.pagination_limit,
            pagination_offset=args.pagination_offset,
            users=[
                UserResultServiceData(
                    first_name=user.first_name,
                    last_name=user.last_name,
                    email=user.email,
                    gender=user.gender,
                    phone=user.phone,
                    public_id=user.public_id,
                )
                for user in users_list
            ],
        )
