class Config(object):

    """Базовая конфигурация приложения."""

    SQLALCHEMY_TRACK_MODIFICATIONS: bool = False
    DEBUG: bool = False
    TESTING: bool = False
    CSRF_ENABLED: bool = True
    SQLALCHEMY_DATABASE_URI: str = 'postgresql+psycopg2://testuser:123@localhost:5432/testdb'


__all__ = ['Config']
