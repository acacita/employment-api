from flask import Blueprint, jsonify
from werkzeug.exceptions import HTTPException

from employment_api.exceptions import ValidationError, DatabaseError, CompanyDoesNotExist

ERRORS_BP = Blueprint('handlers', __name__)


@ERRORS_BP.app_errorhandler(HTTPException)
@ERRORS_BP.app_errorhandler(ValidationError)
@ERRORS_BP.app_errorhandler(DatabaseError)
@ERRORS_BP.app_errorhandler(CompanyDoesNotExist)
def handle_error(error):
    """
    Обработчик кастомных ошибок для представления их в формате json
    :param error:
    :return:
    """
    status_code = error.code or 500
    success = False
    response = {
        'success': success,
        'error': {
            'type': error.__class__.__name__,
            'message': error.description if hasattr(error, 'description') else None,
        },
    }

    return jsonify(response), status_code


@ERRORS_BP.app_errorhandler(Exception)
def handle_unexpected_error(error):
    """
    Обработчик ошибок типа Exception
    :param error:
    :return:
    """
    status_code = 500
    success = False
    message = [str(x) for x in error.args]
    response = {
        'success': success,
        'error': {
            'type': 'Unexpected error occurred',
            'message': message or (error.description if hasattr(error, 'description') else None),
        },
    }

    return jsonify(response), status_code
