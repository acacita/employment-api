from werkzeug.exceptions import HTTPException


class BaseAppException(HTTPException):
    ...


class ValidationError(BaseAppException):
    code = 500
    description = 'Ошибка проверки данных'


class DatabaseError(BaseAppException):
    code = 500
    description = 'Что-то пошло не так'


class CompanyDoesNotExist(BaseAppException):
    code = 400
    description = 'Компания не найдена'


class UserDoesNotExist(BaseAppException):
    code = 400
    description = 'Пользователь не найден'


class DuplicateCompaniesError(BaseAppException):
    code = 400
    description = 'Данный пользователь уже работает в указанных компаниях'
