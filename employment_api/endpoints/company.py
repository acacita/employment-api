import json
from dataclasses import asdict
from typing import Type

import marshmallow_dataclass
from flask import request, Response, Blueprint
from marshmallow import Schema
from marshmallow import ValidationError as MValidationError

from employment_api.exceptions import ValidationError
from employment_api.use_cases import CompanyResultServiceData
from employment_api.use_cases import CompanyServiceData
from employment_api.use_cases import CompanyUseCases
from employment_api.use_cases import GetDeleteCompanyData
from employment_api.use_cases import UpdateCompanyServiceData, CompaniesListServiceData, CompaniesListInput

COMPANY_BP: Blueprint = Blueprint('company_rest_api', __name__)


@COMPANY_BP.route('/company/create', methods=['POST'])
def create_company():
    """
    @api {POST} /company/create Создание компании
    @apiVersion 1.0.0
    @apiName CreateCompany
    @apiGroup company

    @apiParam {str} name Название компании
    @apiParam {str} phone Номер телефона
    @apiParam {str} address Адрес компании
    @apiParam {str} email Электорнный адрес
    @apiParam {str} [website] Сайт
    @apiParam {str} [description] Описание
    @apiParam {str} [logo_url] ссылка на картинку
            @apiParamExample {json} Request-Example:
            {
                "name": "Test Company",
                "phone": "899999999",
                "address": "Test address",
                "email": "email@test.ru",
                "logo_url": "https://logo.com"
            }

        @apiSuccessExample {json} Response-Example:
        {
            "Created company id": 1
        }

    """
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(CompanyServiceData)

    try:
        use_case_data: CompanyServiceData = request_scheme().load(request.json)
    except MValidationError as err:
        raise ValidationError(description=str(err))
    company_id: int = CompanyUseCases.create(use_case_data)
    return Response(json.dumps({'Created company id': company_id}), mimetype='application/json')


@COMPANY_BP.route('/company', methods=['GET'])
def get_company():
    """
    @api {GET} /company Получение данных о компании
    @apiVersion 1.0.0
    @apiName GetCompany
    @apiGroup company

    @apiParam {int} company_id Идентификатор компании
    @apiParamExample {json} Request-Example:
            {"company_id": 1}
    @apiSuccessExample {json} Response-Example:
    {
        "phone": "899999999",
        "name": "Test Company",
        "added_on": null,
        "description": null,
        "email": "email@test.ru",
        "company_id": 1,
        "link": null,
        "address": "Test address",
        "logo_url": "https://logo.com"
    }

    """
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(GetDeleteCompanyData)
    response_scheme: Type[Schema] = marshmallow_dataclass.class_schema(CompanyResultServiceData)

    try:
        args: GetDeleteCompanyData = request_scheme().load(request.json)
    except MValidationError as err:
        raise ValidationError(description=str(err))

    company_data: str = response_scheme().dumps(asdict(CompanyUseCases.get(args.company_id)))

    return Response(company_data, mimetype='application/json')


@COMPANY_BP.route('/company', methods=['DELETE'])
def delete_company():
    """
    @api {DELETE} /company Удаление компании
    @apiVersion 1.0.0
    @apiName DeleteCompany
    @apiGroup company

    @apiParam {int} company_id Идентификатор компании
            @apiParamExample {json} Request-Example:
            {"company_id": 1}
        @apiSuccessExample {json} Response-Example:
        {
            "Deleted company 3": true
        }
    """
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(GetDeleteCompanyData)
    try:
        args: GetDeleteCompanyData = request_scheme().load(request.json)
    except MValidationError as err:
        raise ValidationError(description=str(err))

    deleted: bool = CompanyUseCases.delete(company_id=args.company_id)

    return Response(json.dumps({f'Deleted company {args.company_id}': deleted}), mimetype='application/json')


@COMPANY_BP.route('/company', methods=['POST'])
def update_company():
    """
    @api {POST} /company Обновление данных о компании
    @apiVersion 1.0.0
    @apiName UpdateCompany
    @apiGroup company

    @apiParam {int} company_id Идентификатор компании
    @apiParamExample {json} Request-Example:
        {
            "company_id": 1,
            "website": "website.test"
        }
    @apiSuccessExample {json} Response-Example:
    {
        "logo_url": "https://logo.com",
        "phone": "899999999",
        "added_on": null,
        "link": "website.test",
        "email": "email@test.ru",
        "company_id": 1,
        "name": "Test Company",
        "address": "Test address",
        "description": null
    }

    """
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UpdateCompanyServiceData)
    response_scheme: Type[Schema] = marshmallow_dataclass.class_schema(CompanyResultServiceData)

    try:
        args: UpdateCompanyServiceData = request_scheme().load(request.json)
    except MValidationError as err:
        raise ValidationError(description=str(err))

    updated_company: CompanyResultServiceData = CompanyUseCases.update(args)
    update_company_result: str = response_scheme().dumps(asdict(updated_company))
    return Response(update_company_result, mimetype='application/json')


@COMPANY_BP.route('/company/list', methods=['GET'])
def list_companies():
    """
    @api {GET} /company/list Получение списка неудалённых компаний
    @apiVersion 1.0.0
    @apiName ListCompanies
    @apiGroup company

    @apiParam {int} companies_per_page Колиечество компаний на странице
    @apiParam {int} page Номер страницы
    @apiParamExample {json} Request-Example:
        {
            "companies_per_page" : 2,
            "page": 1
        }
    @apiSuccessExample {json} Response-Example:
    {
        "page": 1,
        "companies": [
            {
                "company_id": 4,
                "logo_url": "123",
                "name": "OOO",
                "email": "email@email.ru",
                "address": "Moskovsky avenue",
                "phone": "894444111",
                "link": null,
                "description": null,
                "added_on": 2020-06-15T09:37:02.752234
            },
            {
                "company_id": 5,
                "logo_url": "https://logo.com",
                "name": "Test Company",
                "email": "email@test.ru",
                "address": "Test address",
                "phone": "899999999",
                "link": "link.com",
                "description": null,
                "added_on": 2020-06-15T09:51:48.257382
            }
        ],
        "companies_per_page": 2
    }

    """

    request_schema: Type[Schema] = marshmallow_dataclass.class_schema(CompaniesListInput)
    response_schema: Type[Schema] = marshmallow_dataclass.class_schema(CompaniesListServiceData)
    try:
        args: CompaniesListInput = request_schema().load(request.json)
    except MValidationError as err:
        raise ValidationError(description=str(err))

    updated_company: CompaniesListServiceData = CompanyUseCases.list_all(args)
    formatted_result = response_schema().dumps(updated_company)
    return Response(formatted_result, mimetype='application/json')
