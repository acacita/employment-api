import json
from dataclasses import asdict
from typing import Dict, Any, Type

import marshmallow_dataclass
from flask import Blueprint, Response, request
from marshmallow import Schema
from marshmallow.exceptions import ValidationError as MarshValidationError

from employment_api.exceptions import ValidationError
from employment_api.use_cases import UserServiceData, UserUseCase, UserServiceUpdateData, UserResultServiceData
from employment_api.use_cases.base_user import (
    UserEmploymentData,
    UserGetDeleteData,
    ListUsersData,
    ListUsersServiceData,
)

USER_BP: Blueprint = Blueprint('user_rest_api', __name__)


@USER_BP.route('/user/create', methods=['POST'])
def create_user():
    """
    @api {POST} /user/create Создание пользователя
    @apiVersion 1.0.0
    @apiName CreateUser
    @apiGroup user

    @apiParam {str} first_name Имя
    @apiParam {str} last_name Фамилия
    @apiParam {str} gender Пол
    @apiParam {str} email Почта
    @apiParam {str} phone Номер телефона
    @apiParam {str} [website] Сайт
    @apiParam {List[int]} [comapny_ids] Список копаний в которые добавить пользователя
    @apiParamExample {json} Request-Example:
        {
            "first_name": "Test",
            "last_name": "Testovich",
            "gender": "male",
            "email": "test@test.ru",
            "phone": "899912345",
            "company_ids": [5]
        }

        @apiSuccessExample {json} Response-Example:
        {
            "Created user id": 3
        }

    """
    user_data_raw: Dict[str, Any] = request.json
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserServiceData)

    try:
        use_case_data: UserServiceData = request_scheme().load(user_data_raw)
    except MarshValidationError as err:
        raise ValidationError(description=str(err))
    user_id: int = UserUseCase.create(use_case_data)
    return Response(json.dumps({'Created user id': user_id}), mimetype='application/json')


@USER_BP.route('/user', methods=['GET'])
def get_user():
    """
    @api {GET} /user Получение пользователя
    @apiVersion 1.0.0
    @apiName GetUser
    @apiGroup user

    @apiParam {int} user_id Идентификатор пользователя
    @apiParamExample {json} Request-Example:
        {
            "first_name": "Test",
            "last_name": "Testovich",
            "gender": "male",
            "email": "test@test.ru",
            "phone": "899912345",
            "company_ids": [5]
        }

    @apiSuccessExample {json} Response-Example:
       {
            "is_employed": true,
            "last_name": "Testovich",
            "first_name": "Test",
            "companies": [
                {
                    "website": "website.com",
                    "company_id": 5,
                    "name": "Test Company",
                    "address": "Test address",
                    "added_on": "2020-06-15T09:51:48.257382",
                    "description": null,
                    "logo_url": "https://logo.com",
                    "phone": "899999999",
                    "email": "email@test.ru"
                }
            ],
            "is_deleted": false,
            "phone": "899912345",
            "email": "test@test.ru",
            "public_id": 3,
            "gender": "male"
        }

    """
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserGetDeleteData)
    try:
        use_case_data: UserGetDeleteData = request_scheme().load(request.json)
    except MarshValidationError as err:
        raise ValidationError(description=str(err))
    user_data: UserResultServiceData = UserUseCase.get(use_case_data.user_id)
    response_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserResultServiceData)
    return Response(response_scheme().dumps(asdict(user_data)), mimetype='application/json')


@USER_BP.route('/user', methods=['POST'])
def update_user():
    """
    @api {POST} /user Обновление пользователя
    @apiVersion 1.0.0
    @apiName UpdateUser
    @apiGroup user

    @apiParam {int} user_id Идентификатор пользователя
    @apiParam [str] first_name Имя
    @apiParam [str] last_name Фамилия
    @apiParam [str] phone Номер телефона
    @apiParam [email] phone Номер телефона
    @apiParamExample {json} Request-Example:
        {
        "user_id": 3,
        "phone": "8999123456",
        "email": "email@email.ru"
    }
    @apiSuccessExample {json} Response-Example:
    {
        "gender": "male",
        "first_name": "Test",
        "companies": [
            {
                "website": "website.com",
                "company_id": 5,
                "name": "Test Company",
                "address": "Test address",
                "added_on": "2020-06-15T09:51:48.257382",
                "description": null,
                "logo_url": "https://logo.com",
                "phone": "899999999",
                "email": "email@test.ru"

            }
        ],
        "is_deleted": false,
        "is_employed": true,
        "last_name": "Testovich",
        "email": "email@email.ru",
        "public_id": 3,
        "phone": "8999123456"
    }

    """
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserServiceUpdateData)
    response_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserResultServiceData)
    try:
        use_case_data: UserServiceUpdateData = request_scheme().load(request.json)
    except MarshValidationError as err:
        raise ValidationError(description=str(err))
    updated_user: UserResultServiceData = UserUseCase.update(use_case_data)
    update_user_result: Dict[str, Any] = response_scheme().dump(asdict(updated_user))
    return Response(json.dumps(update_user_result), mimetype='application/json')


@USER_BP.route('/user/employ', methods=['POST'])
def employ_user():
    """
    @api {POST} /user/employ Добавление пользователя в компании
    @apiVersion 1.0.0
    @apiName EmployUser
    @apiGroup user

    @apiParam {int} user_id Идентификатор пользователя
    @apiParam {List[int]} company_ids Список компаний, в которые добавить пользователя
    @apiParamExample {json} Request-Example:
        {
        "user_id": 3,
        "phone": "8999123456",
        "email": "email@email.ru"
    }
    @apiSuccessExample {json} Response-Example:
    {
    "last_name": "Testovich",
    "first_name": "Test",
    "phone": "8999123456",
    "email": "email@email.ru",
    "is_employed": true,
    "public_id": 3,
    "is_deleted": false,
    "gender": "male",
    "companies": [
            {
                "phone": "899999999",
                "email": "email@test.ru",
                "logo_url": "https://logo.com",
                "name": "Test Company",
                "description": null,
                "company_id": 5,
                "address": "Test address",
                "added_on": "2020-06-15T09:51:48.257382",
                "website": "website.com"
            },
            {
                "phone": "1545454",
                "email": "email@appkode.ru",
                "logo_url": null,
                "name": "Test Company",
                "description": null,
                "company_id": 6,
                "address": "Kaliningrad",
                "added_on": "2020-06-15T10:45:16.616582",
                "website": null
            }
        ]
    }

    """
    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserEmploymentData)
    response_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserResultServiceData)
    try:
        use_case_data: UserEmploymentData = request_scheme().load(request.json)
    except MarshValidationError as err:
        raise ValidationError(description=str(err))
    updated_user: UserResultServiceData = UserUseCase.employ(use_case_data)
    update_user_result: Dict[str, Any] = response_scheme().dump(asdict(updated_user))
    return Response(json.dumps(update_user_result), mimetype='application/json')


@USER_BP.route('/user', methods=['DELETE'])
def delete_user():
    """
    @api {DELETE} /user Удаление пользователя
    @apiVersion 1.0.0
    @apiName DeleteUser
    @apiGroup user

    @apiParam {int} user_id Идентификатор пользователя
    @apiParamExample {json} Request-Example:
    {
        "user_id": "3"
    }
    @apiSuccessExample {json} Response-Example:
    {
    "Deleted user 3": true
    }
    """

    request_scheme: Type[Schema] = marshmallow_dataclass.class_schema(UserGetDeleteData)
    try:
        use_case_data: UserGetDeleteData = request_scheme().load(request.json)
    except MarshValidationError as err:
        raise ValidationError(description=str(err))
    success: bool = UserUseCase.delete(use_case_data.user_id)
    return Response(json.dumps({f'Deleted user {use_case_data.user_id}': success}), mimetype='application/json')


@USER_BP.route('/users/list', methods=['GET'])
def list_users():
    """
    @api {GET} /user/list Получение списка пользователей
    @apiVersion 1.0.0
    @apiName ListUsers
    @apiGroup user

    @apiParam {int} pagination_offset Номер страницы
    @apiParam {int} pagination_limit Количество записей на странице
    @apiParamExample {json} Request-Example:
        {
            "pagination_offset" : 0,
            "pagination_limit": 2
        }
    @apiSuccessExample {json} Response-Example:
    {
        "pagination_offset": 1,
        "users": [
                    {
                "public_id": 1,
                "phone": "855512345",
                "is_deleted": false,
                "email": "mb@meh.ru",
                "gender": "female",
                "is_employed": false,
                "last_name": "Belozerova",
                "companies": null,
                "first_name": "Maria"
            },
            {
                "public_id": 2,
                "phone": "899912345",
                "is_deleted": false,
                "email": "test@test.ru",
                "gender": "male",
                "is_employed": false,
                "last_name": "Testovich",
                "companies": null,
                "first_name": "Test"
            }

        ],
        "pagination_limit": 2
    }

    """

    request_schema: Type[Schema] = marshmallow_dataclass.class_schema(ListUsersData)
    response_schema: Type[Schema] = marshmallow_dataclass.class_schema(ListUsersServiceData)
    try:
        args: ListUsersData = request_schema().load(request.json)
    except MarshValidationError as err:
        raise ValidationError(description=str(err))

    listed_users: ListUsersServiceData = UserUseCase.list_all(args)
    formatted_result = response_schema().dumps(listed_users)

    return Response(formatted_result, mimetype='application/json')
