from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class CompanyStorageData:
    """
    Данные о компании для обработки в бд
    """

    name: str
    phone: str
    address: str
    email: str
    company_id: Optional[int] = None
    added_on: Optional[datetime] = None
    logo_url: Optional[str] = None
    website: Optional[str] = None
    description: Optional[str] = None
