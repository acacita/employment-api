from dataclasses import dataclass, field
from typing import Any, Optional, List

from .base_company_repository import CompanyStorageData


@dataclass
class UserStorageData:
    """
    Данные о пользователе, хранящиеся в бд
    """

    first_name: str
    last_name: str
    gender: str
    email: str
    phone: str
    public_id: int = 0
    company_ids: List[Any] = field(default_factory=list)
    companies: Optional[List[CompanyStorageData]] = None
    is_employed: bool = False
    admin: bool = False

    def __post_init__(self):
        self.is_employed = bool(self.company_ids) or bool(self.companies)
