from typing import Optional, Any, Dict, List

from sqlalchemy.exc import IntegrityError, SQLAlchemyError  # type: ignore

from employment_api.exceptions import CompanyDoesNotExist, DatabaseError
from employment_api.models import Company
from .base_company_repository import CompanyStorageData
from .base_orm_storage import DatabaseSessionScope


class CompanyRepository:
    def __init__(self, db: DatabaseSessionScope):
        self.session = db.session

    @staticmethod
    def _check_exists(company_id: int) -> Company:
        """
        Проверка на существование компании
        :param company_id:
        :return: сущность компании
        """
        company: Optional[Company] = Company.query.get(company_id)
        if not company or company.deleted:
            raise CompanyDoesNotExist
        return company

    def get_company_list(self, data: List[str]) -> Optional[List[Company]]:
        """
        Поиск компаний с соответствующими id
        :param data: список id компаний
        :return: Список данных о компаниях
        :raises: CompanyDoesNotExist
        """
        company_list: Optional[List[Company]] = self.session.query(Company).filter(Company.id.in_(data)).all()
        if not company_list:
            raise CompanyDoesNotExist
        return company_list

    def create(self, data: CompanyStorageData) -> int:
        """
        Создание новой компании в бд
        :param data:
        :return: сгенерированный id компании
        """
        company: Company = Company(
            name=data.name,
            phone=data.phone,
            logo_url=data.logo_url,
            description=data.description,
            address=data.address,
            email=data.email,
            website=data.website,
        )
        try:
            self.session.add(company)
            self.session.commit()
        except (IntegrityError, SQLAlchemyError) as err:
            raise DatabaseError(description=str(err))
        return company.id

    def update(self, company_id: int, update_data: Dict[str, Any]) -> CompanyStorageData:
        """
        Обновление данных о компании
        :param company_id:
        :param update_data:
        :return:
        """
        company_to_update: Company = CompanyRepository._check_exists(company_id=company_id)
        if company_to_update:
            Company.query.filter_by(id=company_id).update(update_data)
        try:
            self.session.commit()
        except (IntegrityError, SQLAlchemyError, Exception) as ex:
            raise DatabaseError(description=str(ex))
        return CompanyStorageData(
            address=company_to_update.address,
            email=company_to_update.email,
            name=company_to_update.name,
            phone=company_to_update.phone,
            logo_url=company_to_update.logo_url,
            website=company_to_update.website,
            description=company_to_update.description,
        )

    @staticmethod
    def get(public_id: int) -> CompanyStorageData:
        """
        Получение компании из бд
        :param public_id: уникальный идентификатор компании
        :return:
        :raises: CompanyDoesNotExist
        """
        company: Company = CompanyRepository._check_exists(company_id=public_id)
        return CompanyStorageData(
            name=company.name,
            phone=company.phone,
            address=company.address,
            email=company.email,
            logo_url=company.logo_url,
            website=company.website,
            description=company.description,
            company_id=company.id,
        )

    def delete(self, public_id: int) -> bool:
        """
        Soft delete компании
        :param public_id:
        :return:
        """
        company: Company = CompanyRepository._check_exists(company_id=public_id)
        company.deleted = True
        self.session.commit()
        return True

    def company_list(self, page_number: int, companies_per_page: int) -> List[CompanyStorageData]:
        """
        Метод для получения списка всех компаний с пагинацией
        :param page_number:
        :param companies_per_page:
        :return:
        """
        companies: List[Company] = self.session.query(Company).filter(Company.deleted.is_(False)).paginate(
            page_number, companies_per_page, False
        ).items
        return [
            CompanyStorageData(
                name=company.name,
                website=company.website,
                logo_url=company.logo_url,
                description=company.description,
                email=company.email,
                phone=company.phone,
                address=company.address,
                company_id=company.id,
                added_on=company.added_on,
            )
            for company in companies
        ]
