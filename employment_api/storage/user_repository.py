from typing import Any, Dict, List, Optional

from sqlalchemy.exc import IntegrityError, SQLAlchemyError  # type: ignore
from sqlalchemy.orm import Session  # type: ignore

from employment_api.exceptions import DatabaseError, CompanyDoesNotExist
from employment_api.exceptions import DuplicateCompaniesError
from employment_api.exceptions import UserDoesNotExist
from employment_api.models import User, Company
from .base_company_repository import CompanyStorageData
from .base_orm_storage import DatabaseSessionScope
from .base_user_repository import UserStorageData


class UserRepository:
    def __init__(self, db: DatabaseSessionScope):
        self.session: Session = db.session

    def _check_companies_exist(self, company_ids: List[int]) -> Optional[List[Company]]:
        """
        Проверка на существование компаний с соответствующими id
        :param company_ids: список id компаний
        :return: Список данных о компаниях
        :raises: CompanyDoesNotExist если хотя бы одна компания не существует
        """
        company_list: Optional[List[Company]] = self.session.query(Company).filter(Company.id.in_(company_ids)).all()
        if not company_list:
            raise CompanyDoesNotExist
        return [company for company in company_list if not company.deleted]

    @staticmethod
    def _check_user_exists(user_id: int) -> User:
        """
        Проверка на существование и не удалённость юзера
        :param user_id:
        :return:
        :raises: UserDoesNotExist
        """
        existing_user: Optional[User] = User.query.get(user_id)
        if not existing_user or existing_user.deleted:
            raise UserDoesNotExist
        return existing_user

    @staticmethod
    def _prepare_data(user_data: User, companies: Optional[List[Company]] = None) -> UserStorageData:
        """
        :param user_data:
        :param companies:
        :return:
        """
        formatted_companies: Optional[List[CompanyStorageData]] = [
            CompanyStorageData(
                name=company.name,
                phone=company.phone,
                address=company.address,
                email=company.email,
                website=company.website,
                logo_url=company.logo_url,
                added_on=company.added_on,
                company_id=company.id,
                description=company.description,
            )
            for company in companies
        ] if companies else None

        return UserStorageData(
            first_name=user_data.firstname,
            last_name=user_data.lastname,
            email=user_data.email,
            gender=user_data.gender,
            phone=user_data.phone,
            public_id=user_data.id,
            companies=formatted_companies,
        )

    def create(self, data: UserStorageData) -> int:
        """
        Создание нового пользователя в бд
        :param data:
        :return: сгенерированный id пользователя
        :raises: DatabaseError
        """
        new_user: User = User(
            firstname=data.first_name,
            lastname=data.last_name,
            is_employed=data.is_employed,
            admin=data.admin,
            email=data.email,
            phone=data.phone,
            gender=data.gender,
        )
        if data.company_ids:
            companies: Optional[List[Company]] = self._check_companies_exist(company_ids=data.company_ids)
            if not companies:
                raise CompanyDoesNotExist
            new_user.companies.extend(companies)
        try:
            self.session.add(new_user)
            self.session.commit()
        except (IntegrityError, SQLAlchemyError, Exception) as err:
            raise DatabaseError(description=str(err))
        return new_user.id

    def employ(self, public_id: int, company_ids: List[int]) -> UserStorageData:
        """
        Добавление пользователя в компании
        :param public_id: id пользователя
        :param company_ids: список id компаний
        :return: данные о пользователе и компаниях, в которых он теперь работает
        :raises: DuplicateCompaniesError если пользователь работает в указанных компаниях
        """
        existing_user: User = UserRepository._check_user_exists(public_id)
        new_companies: Optional[List[Company]] = self._check_companies_exist(company_ids=company_ids)
        if not new_companies:
            raise CompanyDoesNotExist
        try:
            existing_user.companies.extend(new_companies)
            self.session.commit()
        except (IntegrityError, Exception):
            raise DuplicateCompaniesError()
        all_companies: List[Company] = [
            company for company in existing_user.companies.all() if company and not company.deleted
        ]
        return UserRepository._prepare_data(existing_user, all_companies)

    def update(self, user_id: int, update_data: Dict[str, Any]) -> UserStorageData:
        """
        Обновление некоторых данных о пользователе.
        :param user_id:
        :param update_data:
        :return:
        :raises: UserDoesNotExist
        :raises: DuplicateCompaniesError
        """
        updated_user: User = UserRepository._check_user_exists(user_id=user_id)
        if update_data:
            User.query.filter_by(id=user_id).update(update_data)
        try:
            self.session.commit()
        except (IntegrityError, SQLAlchemyError, Exception) as ex:
            raise DatabaseError(description=str(ex))
        companies: Optional[List[Company]] = [
            company for company in updated_user.companies.all() if company and not company.deleted
        ]
        return UserRepository._prepare_data(updated_user, companies)

    @staticmethod
    def get(public_id: int) -> UserStorageData:
        """
        Получение пользователя из бд по уникальному id
        :param public_id:
        :return:
        :raises:
        """
        existing_user: User = UserRepository._check_user_exists(user_id=public_id)
        companies: List[Company] = [
            company for company in existing_user.companies.all() if company and not company.deleted
        ]

        return UserRepository._prepare_data(existing_user, companies)

    def delete(self, public_id: int) -> bool:
        """
        Удаление пользователя
        :param public_id:
        :return:
        :raises: DatabaseError
        """
        existing_user: User = UserRepository._check_user_exists(user_id=public_id)
        existing_user.deleted = True
        try:
            self.session.commit()
        except (IntegrityError, Exception) as ex:
            raise DatabaseError(description=str(ex))
        return True

    def list_all(self, pagination_limit: int, pagination_offset: int):
        """
        Получение списка всех пользователей с пагинацией
        :param pagination_limit:
        :param pagination_offset:
        :return:
        """
        users: List[User] = self.session.query(User).filter(User.deleted.is_(False)).paginate(
            pagination_offset, pagination_limit, False
        ).items
        return [
            UserStorageData(
                first_name=user.firstname,
                last_name=user.lastname,
                email=user.email,
                gender=user.gender,
                phone=user.phone,
                public_id=user.id,
            )
            for user in users
        ]
