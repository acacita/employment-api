from employment_api.extensions import db


class DatabaseSessionScope:
    """
    Контекстный менеджер для работы с коннектами бд и сессиями с ORM
    """

    def __init__(self):
        self.db = db
        self.session = self.db.session()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.close()
