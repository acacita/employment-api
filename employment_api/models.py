from datetime import datetime

from employment_api.extensions import db

users_companies = db.Table(
    'users_companies',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('company_id', db.Integer, db.ForeignKey('company.id'), primary_key=True),
)


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    firstname = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    is_employed = db.Column(db.Boolean, default=False)
    deleted = db.Column(db.Boolean(), default=False)
    email = db.Column(db.String(60), nullable=True)
    phone = db.Column(db.String(60), nullable=True)
    gender = db.Column(db.Enum('male', 'female', 'na', name='genders'))

    companies = db.relationship(
        'Company',
        secondary=users_companies,
        lazy='dynamic',
        backref=db.backref('users', lazy=True),
        cascade='all, delete-orphan',
        single_parent=True,
    )


class Company(db.Model):
    __tablename__ = 'company'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    phone = db.Column(db.String(20), nullable=False)
    address = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False)
    website = db.Column(db.String(100), nullable=True)
    logo_url = db.Column(db.String(100), nullable=True)
    description = db.Column(db.String(100), nullable=True)
    added_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    deleted = db.Column(db.Boolean(), default=False)


__all__ = ['User', 'Company', 'users_companies']
